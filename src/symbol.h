#ifndef _SYMBOL_H_
#define _SYMBOL_H_

void del_portf_cb(GtkWidget *widget, gpointer data);
void add_symbol(GtkWidget *widget, gpointer data);
void get_chart_cb(GtkWidget *widget, gpointer data);
void del_symb_cb(GtkWidget *widget, gpointer data);
void cancelbt_cb(GtkWidget *widget, gpointer data);
void lookup_cb(GtkWidget *widget, gpointer data);
void file_quit_callback (GtkWidget *widget, gpointer data);
gint update_cb(GtkWidget *widget, gpointer data);
void close_portf(GtkWidget *widget, gpointer data);
void save_cb(GtkWidget *widget, gpointer data);

#endif
