#include "gnome-pm.h"

static void create_currlist(void) {
  curr_item *item;
  
  item = NULL;
}

void curr_conv_cb(GtkWidget *widget, gpointer data) {
  GtkWidget *dialog, *entry, *label, *hbox;
  
  dialog = gnome_dialog_new(_("Currency Converter"), GNOME_STOCK_BUTTON_OK,
  	GNOME_STOCK_BUTTON_CANCEL, NULL);
  create_currlist();
  hbox = gtk_hbox_new(FALSE, FALSE);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), hbox);
    gtk_widget_show(hbox);
  label = gtk_label_new(g_strconcat(_("Convert:"), NULL));
    gtk_container_add(GTK_CONTAINER(hbox), label);
    gtk_widget_show(label);
  entry = gtk_entry_new_with_max_length(32);
    gtk_container_add(GTK_CONTAINER(hbox), entry);
    gtk_widget_set_usize(entry, 24, 0);
    gtk_widget_show(entry);
  gnome_dialog_set_default(GNOME_DIALOG(dialog), 0);
  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), GTK_EDITABLE(entry));
  gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(cancelbt_cb), NULL);
  gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb), NULL);
  gtk_widget_grab_focus(GTK_WIDGET(GTK_EDITABLE(entry)));
  gtk_widget_show(dialog);
}
