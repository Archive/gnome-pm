#ifndef _CURR_CONV_H_
#define _CURR_CONV_H_

typedef struct {
	gchar *country;
	gchar *currid;
} curr_item; 

GList *curr_list;

void curr_conv_cb(GtkWidget *widget, gpointer data);

#endif