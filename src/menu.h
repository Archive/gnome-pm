/* menu.h - for now just show createMenu */


#ifndef __GNOMEMENU_H__
#define __GNOMEMENU_H__


void createMenu(GtkWidget *frame);
void no_pf_menus(void);
void pf_menus(void);
void select_row_cb(GtkWidget *clist, gint row, gint col, gpointer event);
void unselect_row_cb(GtkWidget *clist, gint row, gint col, gpointer event);

#endif /* __GNOMEMENU_H__ */

