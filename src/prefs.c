#include "gnome-pm.h"

static GtkWidget *propbox = NULL;
int TRememberPosition;
int TPromptOnQuit, TSndEnabled, TLoadPrev;
GtkWidget *yhoo_id, *yhoo_pw, *yhoo_host, *gProxyHost, *gProxyUser, *gProxyPass;
GtkWidget *upd_spin;

static void prevfile_cb(GtkWidget *widget, gpointer data)
{
  TLoadPrev = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void sndenable_cb(GtkWidget *widget, gpointer data)
{
  TSndEnabled = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void rememberpos_cb(GtkWidget *widget, gpointer data)
{
  TRememberPosition = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void promptonquit_cb(GtkWidget *widget, gpointer data)
{
  TPromptOnQuit = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void yahoo_id_cb( GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void yahoo_pw_cb( GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void yahoo_host_cb(GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void apply_cb(GtkWidget *w, gint pagenum, gpointer data)
{
  if ((pagenum == 0) || (pagenum == -1)) {
	g_free(ProxyHost);
	g_free(ProxyUser);
	g_free(ProxyPass);
	g_free(yh_id);
	g_free(yh_pw);
	g_free(yh_host);
	
	yh_id = g_strdup(gtk_entry_get_text(GTK_ENTRY(yhoo_id)));
	yh_pw = g_strdup(gtk_entry_get_text(GTK_ENTRY(yhoo_pw)));
	yh_host = g_strdup(gtk_entry_get_text(GTK_ENTRY(yhoo_host)));
	ProxyHost = g_strdup(gtk_entry_get_text(GTK_ENTRY(gProxyHost)));
 	ProxyUser = g_strdup(gtk_entry_get_text(GTK_ENTRY(gProxyUser)));
 	ProxyPass = g_strdup(gtk_entry_get_text(GTK_ENTRY(gProxyPass)));
  	UpdateTime = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(upd_spin));
  	
	if(update_id) {
	  gtk_timeout_remove(update_id);
	}
	if(UpdateTime > 0 && pfname[0] != '\0') {
	  update_id = gtk_timeout_add((UpdateTime * 60 * 1000), (GtkFunction) update_cb, NULL);
	}	
  	gnome_config_push_prefix("GnomePM/");
  	gnome_config_set_string("Yahoo/YahooID", yh_id);
  	gnome_config_set_string("Yahoo/YahooPW", yh_pw);
	gnome_config_set_string("Yahoo/YahooHost", yh_host);
	gnome_config_set_string("Yahoo/ProxyHost", ProxyHost);
	gnome_config_set_string("Yahoo/ProxyUser", ProxyUser);
	gnome_config_set_string("Yahoo/ProxyPass", ProxyPass);
	gnome_config_set_int("Options/UpdateTime", UpdateTime);

  	gnome_config_sync();
  	gnome_config_pop_prefix();
  }
  if((pagenum == 1)||(pagenum == -1)) {
    SoundEnabled = TSndEnabled;
    RememberPosition = TRememberPosition;
    PromptOnQuit = TPromptOnQuit;
    LoadPrev = TLoadPrev;
    
    gnome_config_push_prefix("GnomePM/");
    gnome_config_set_int("Sound/Enabled", SoundEnabled);
    gnome_config_set_int("Options/RememberPosition",RememberPosition);
    gnome_config_set_int("Options/PromptOnQuit",PromptOnQuit);
    gnome_config_set_int("Options/LoadPrev", LoadPrev);
    
    gnome_config_sync();
    gnome_config_pop_prefix();
  }
}

static void destroy_cb(GtkWidget *w, gpointer data)
{
  gtk_widget_destroy(propbox);
  propbox = NULL;
}

void pref_dlg_cb(GtkWidget *widget, gpointer data) {
  GtkWidget *cpage;
  GtkWidget *hbox, *vbox, *vbox2, *frame;
  GtkWidget *chkbox;
  GtkWidget *label, *textbox;
  GtkAdjustment *spin_adjust;
  
  if(propbox) return;
  
  TRememberPosition = RememberPosition;
  TPromptOnQuit = PromptOnQuit;
  TSndEnabled = SoundEnabled;

  propbox = gnome_property_box_new();
  gtk_window_set_modal(GTK_WINDOW(propbox), TRUE);
  gtk_window_set_title(GTK_WINDOW(&GNOME_PROPERTY_BOX(propbox)->dialog.window), _("GnomePM Preferences"));
   
  cpage = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_BIG);
  
  hbox = gtk_hbox_new(FALSE, 0);
  vbox = gtk_vbox_new(TRUE, 0);
  vbox2 = gtk_vbox_new(TRUE, 0);

  frame = gtk_frame_new("Yahoo!");
  gtk_box_pack_start(GTK_BOX(cpage), frame, FALSE, FALSE, GNOME_PAD_BIG);

  gtk_container_set_border_width(GTK_CONTAINER(frame), GNOME_PAD_BIG);
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, GNOME_PAD_SMALL);

  label = gtk_label_new("Yahoo! ID:");
  textbox = gtk_entry_new();
  yhoo_id = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), yh_id);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
             (GtkSignalFunc) yahoo_id_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);

  label = gtk_label_new("Yahoo! Password:");
  textbox = gtk_entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(textbox), FALSE);
  yhoo_pw = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), yh_pw);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
             (GtkSignalFunc) yahoo_pw_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);

  label = gtk_label_new("Yahoo! Host:");
  textbox = gtk_entry_new();
  yhoo_host = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), yh_host);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
	     (GtkSignalFunc) yahoo_host_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show(textbox);
  gtk_widget_show(label);
  
  label = gtk_label_new("Auto Update:");
  
  spin_adjust = GTK_ADJUSTMENT(gtk_adjustment_new(5.0, 0.0, 60.0, 1.0, 5.0, 5.0));
  textbox = gtk_spin_button_new(spin_adjust, 1.0, 0);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(textbox), (gfloat) (UpdateTime));
  upd_spin = textbox;
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show(textbox);
  gtk_widget_show(label);

  gtk_widget_show(vbox);
  gtk_widget_show(vbox2);
  gtk_widget_show(hbox);
  gtk_widget_show(frame);

  frame = gtk_frame_new("Proxy");
  gtk_box_pack_start(GTK_BOX(cpage), frame, FALSE, FALSE, GNOME_PAD_SMALL);

  hbox = gtk_hbox_new(FALSE, 0);
  vbox = gtk_vbox_new(TRUE, 0);
  vbox2 = gtk_vbox_new(TRUE, 0);

  gtk_container_add(GTK_CONTAINER(frame), hbox);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, GNOME_PAD_SMALL);

  label = gtk_label_new("Proxy Host:");
  textbox = gtk_entry_new();
  gProxyHost = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), ProxyHost);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
             (GtkSignalFunc) yahoo_id_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);

  label = gtk_label_new("Proxy Username:");
  textbox = gtk_entry_new();
  gProxyUser = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), ProxyUser);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
             (GtkSignalFunc) yahoo_id_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);

  label = gtk_label_new("Proxy Password:");
  textbox = gtk_entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(textbox), FALSE);
  gProxyPass = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox), ProxyPass);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
             (GtkSignalFunc) yahoo_pw_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);

  gtk_widget_show(vbox);
  gtk_widget_show(vbox2);
  gtk_widget_show(hbox);
  gtk_widget_show(frame);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Network"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), cpage, label);


  cpage = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);
  
  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Enable Sound"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TSndEnabled;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
  	(GtkSignalFunc)sndenable_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox);

  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Remember Position"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TRememberPosition;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
  	(GtkSignalFunc)rememberpos_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox);
  
  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Prompt on Quit"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TPromptOnQuit;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
  	(GtkSignalFunc)promptonquit_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox);
  
  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Load Last Open"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TLoadPrev;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
  	(GtkSignalFunc)prevfile_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Misc"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), cpage, label);
  
  gtk_signal_connect(GTK_OBJECT(propbox), "destroy",
  	GTK_SIGNAL_FUNC(destroy_cb), NULL);
  gtk_signal_connect(GTK_OBJECT(propbox), "apply",
  	GTK_SIGNAL_FUNC(apply_cb), NULL);
  gtk_widget_show(propbox);
}
