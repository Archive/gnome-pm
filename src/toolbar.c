#include "gnome-pm.h"

/* The toolbar buttons */

static GnomeUIInfo main_toolbar[]= {
  {
    GNOME_APP_UI_ITEM,
    _("New"), _("Create a new portfolio"),
    add_portfolio, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW,
    'N', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("Open"), _("Open a Portfolio"),
    open_portf, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN,
    'O', GDK_CONTROL_MASK, NULL
  },
  { 
    GNOME_APP_UI_ITEM,
    _("Close"), _("Close the Current Portfolio"),
    close_portf, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CLOSE,
    'W', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("Save"), _("Save Current Portfolio"),
    save_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE,
    'D', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
	GNOME_APP_UI_ITEM,
	_("Add"), _("Add a new symbol"),
	add_symbol, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_ADD,
	'A', GDK_CONTROL_MASK, NULL
  },
  {
	GNOME_APP_UI_ITEM,
	_("Remove"), _("Remove the selected symbol"),
	del_symb_cb, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REMOVE,
	'R', GDK_CONTROL_MASK, NULL
  },
  { 
    GNOME_APP_UI_ITEM,
    _("Update"), _("Update Listings"),
    update_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REFRESH,
    'U', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("Lookup"), _("Lookup a symbol"),
    lookup_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SEARCH,
    'L', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

void set_symbol_tools(gboolean sense) {
  gtk_widget_set_sensitive(main_toolbar[6].widget, sense);
}

void set_pf_tools(gboolean sense) {
  gtk_widget_set_sensitive(main_toolbar[2].widget, sense);
  gtk_widget_set_sensitive(main_toolbar[3].widget, sense);
  gtk_widget_set_sensitive(main_toolbar[5].widget, sense);
  gtk_widget_set_sensitive(main_toolbar[7].widget, sense);
  gtk_widget_set_sensitive(main_toolbar[8].widget, sense);
}  

/* Create the toolbar */

void createToolbar(GtkWidget *frame)    
{
  gnome_app_create_toolbar (GNOME_APP (frame), main_toolbar);
  set_pf_tools(FALSE);
  set_symbol_tools(FALSE);
}
