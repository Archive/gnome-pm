#ifndef _STOCK_LIST_H_
#define _STOCK_LIST_H_

void smbl_up(GtkWidget *widget, gpointer data);
void smbl_dn(GtkWidget *widget, gpointer data);
void smbl_sort(GtkWidget *widget, gpointer data);
void open_portf(GtkWidget *widget, gpointer data);
void createClist(GtkWidget *frame);
void fill_clist(GtkWidget *clist, char *portf_name);
void refresh_clist(GtkWidget *widget, gpointer data);
void set_symbol_popup(gboolean sense);
void set_symbol_tools(gboolean sense);
void set_pf_tools(gboolean sense);
void add_portfolio(GtkWidget *widget, gpointer data);
void get_update_symb(gchar *portf_name);
void createToolbar(GtkWidget *frame);

#endif
