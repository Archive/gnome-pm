#include "gnome-pm.h"
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>

GtkSortType sort_type = GTK_SORT_ASCENDING;
static GtkWidget *list_popup;

/*
	UPDATE URL:
http://q22.yahoo.com/dj/quotes.csv?symbols=(SYMBOL+SYMBOL)&format=(Format string)
*/

static GnomeUIInfo symbol_popup[]=
{
  {
    GNOME_APP_UI_ITEM,
    _("_Add..."), _("Add Symbol to Portfolio"),
    add_symbol, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    'A', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Remove"), _("Remove Symbol from Portfolio"),
    del_symb_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TRASH,
    'R', GDK_CONTROL_MASK, NULL
  },
    GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    _("Symbol _Lookup"), _("Lookup a Symbol"),
    lookup_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SEARCH,
    'L', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
	GNOME_APP_UI_ITEM,
	_("Sort _Symbols"), _("Sort the portfolio"),
	smbl_sort, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_INDEX,
	'S', GDK_SHIFT_MASK, NULL
  },
  {
	GNOME_APP_UI_ITEM,
	_("Move _Up"), _("Move the symbol up one place"),
	smbl_up, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UP,
	'U', GDK_SHIFT_MASK, NULL
  },
  {
	GNOME_APP_UI_ITEM,
	_("Move _Down"), _("Move the symbol down one place"),
	smbl_dn, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_DOWN,
	'D', GDK_SHIFT_MASK, NULL
  },
   /*
  {
    GNOME_APP_UI_ITEM,
    _("Mod_ify..."), _("Modify Symbol Settings"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP,
    'I', GDK_CONTROL_MASK, NULL
  },
  */
  GNOMEUIINFO_END
};

void set_symbol_popup(gboolean sense) {
  gtk_widget_set_sensitive(symbol_popup[1].widget, sense);
  gtk_widget_set_sensitive(symbol_popup[6].widget, sense);
  gtk_widget_set_sensitive(symbol_popup[7].widget, sense);
}

void smbl_up(GtkWidget *widget, gpointer data) {
		gint sel_row;
		
		if(GTK_CLIST(stock_list)->selection == NULL) {
				return;
		}

		sel_row = GPOINTER_TO_INT(GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection));

		if(sel_row == 0)
				return;
		gtk_clist_swap_rows(GTK_CLIST(stock_list), sel_row, sel_row - 1);
}

void smbl_dn(GtkWidget *widget, gpointer data) {
		gint sel_row;
		
		if(GTK_CLIST(stock_list)->selection == NULL) {
				return;
		}

		sel_row = GPOINTER_TO_INT(GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection));

		if(sel_row == (GTK_CLIST(stock_list)->rows - 1))
				return;
		gtk_clist_swap_rows(GTK_CLIST(stock_list), sel_row, sel_row + 1);
}

#define TMP_BUFFER_SIZE 255
void fill_clist(GtkWidget *clist, char *portf_name){
/*
	GtkStyle *list_style;
	GdkColor col;
*/
	char *buffer;
	gchar **list_nfo;
	char *homedir;
	FILE *file;
	int i, y, z, x = 0;
	gulong tlong, filesize, bytecount = 0;
	gfloat tfloat = 0.0;
	
	gtk_clist_freeze(GTK_CLIST(clist));
	
	homedir = (char *)g_malloc(strlen(getenv("HOME"))+strlen("/.gnome-pm/quotes/") + strlen(portf_name) + 5);
	sprintf(homedir, "%s/.gnome-pm/quotes/%s", getenv("HOME"), portf_name);

	if ((file = fopen(homedir, "r")) != NULL) {
	gtk_clist_clear(GTK_CLIST(clist));
	tlong = ftell(file);
	fseek(file, 0L, SEEK_END);
	filesize = ftell(file) - tlong;
	bytecount = 0;
	rewind(file);
	
	buffer = (char *)g_malloc(sizeof(char)*TMP_BUFFER_SIZE);
	gnome_appbar_set_status (GNOME_APPBAR(statusbar), _("Reading quotes..."));
	while(fgets(buffer, TMP_BUFFER_SIZE, file)) {
		if(strstr(buffer, "Missing Symbols List.") != NULL) {
			break;
		}
		list_nfo = (char **)g_malloc(strlen(buffer) + 1);
		list_nfo[0] = strtok(buffer, ",");
		for (i = 1; i < NUMCOLS; i++) {
			list_nfo[i] = strtok(NULL, ",");
		}
		for(y = 0; y < NUMCOLS; y++){
			for(i = 0; i < strlen(list_nfo[y]); i++){
				if(list_nfo[y][i] == '\"') {
					list_nfo[y][i] = list_nfo[y][i + 1];
				}
			}
			if( y == 0 || y == 1 || y == 3 || y == 9 || y == 10 ) {
				for(z = 0; z < strlen(list_nfo[y]); z++){
					list_nfo[y][z] = list_nfo[y][z + 1];
				}
			}
		}
		gtk_clist_append(GTK_CLIST(clist), list_nfo);
/*
		for(y = 0; y < NUMCOLS; y++) {
			if(list_nfo[y][0] == '-') {
				col.red = 0xffff;
				col.green = 0;
				col.blue = 0;
			list_style = gtk_style_copy(clist->style);
			list_style->fg[GTK_STATE_NORMAL] = col;
			list_style->fg[GTK_STATE_SELECTED] = col;
			gtk_clist_set_cell_style(GTK_CLIST(clist),
				GTK_CLIST(clist)->rows-1, y, list_style);
			}
			else if(list_nfo[y][0] == '+') {
				col.red = 0;
				col.green = 0x2e8b57;
				col.blue = 0;
			list_style = gtk_style_copy(clist->style);
			list_style->fg[GTK_STATE_NORMAL] = col;
			list_style->fg[GTK_STATE_SELECTED] = col;
			gtk_clist_set_cell_style(GTK_CLIST(clist),
				GTK_CLIST(clist)->rows-1, y, list_style);
			}
		}
*/
		bytecount = bytecount + strlen(buffer);
		tfloat = ((float )bytecount/filesize);
		tfloat = tfloat * 50;
		if (tfloat > 1.0)
			tfloat = 1.0;
		if (tfloat < 0.0)
			tfloat = 0.0;
		gnome_appbar_set_progress(GNOME_APPBAR(statusbar), tfloat);
      	while (gtk_events_pending ())
		gtk_main_iteration ();
		g_free(list_nfo);
		x++;
	}
	g_free(buffer);
	fclose(file);
	while(gtk_events_pending())
		gtk_main_iteration();
	gnome_appbar_set_progress(GNOME_APPBAR(statusbar), 0.0);
	gnome_appbar_set_status (GNOME_APPBAR(statusbar), "");
	}
	else {
	}
	g_free(homedir);
	while(gtk_events_pending())
		gtk_main_iteration();
	gtk_clist_thaw(GTK_CLIST(clist));
}

void smbl_sort(GtkWidget *widget, gpointer data) {
		gchar *blah;
		
		gnome_config_push_prefix("GnomePM/");
		blah = gnome_config_get_string("Options/PfName=");
		gnome_config_pop_prefix();
		
		if(blah[0] == '\0') {
			return;
		}
		if(sort_type == GTK_SORT_ASCENDING) {
			sort_type = GTK_SORT_DESCENDING;
			gtk_clist_set_sort_type(GTK_CLIST(stock_list), sort_type);
			gtk_clist_sort(GTK_CLIST(stock_list));
		}
		else {
			sort_type = GTK_SORT_ASCENDING;
			gtk_clist_set_sort_type(GTK_CLIST(stock_list), sort_type);
			gtk_clist_sort(GTK_CLIST(stock_list));
		}
}

void createClist(GtkWidget *frame) {

	GtkWidget *scroll_win, *colbtn;
	int i;
	gchar *slist_titles[] = 
	{ _("Symbol"), _("Time"), _("Last"), _("Change"), _("Volume"),
		_("Bid"), _("Ask"), _("Prev. Close"), _("Open"), _("Day's Range"),
		_("52-week Range"), _("Earn/Share"), _("P/E Ratio"), _("Yield") };
	
	scroll_win = gtk_scrolled_window_new(NULL,NULL);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_win),
				GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
		gtk_container_add(GTK_CONTAINER(frame), scroll_win);
		gtk_widget_show(scroll_win);
	stock_list = gtk_clist_new_with_titles(NUMCOLS, slist_titles);
	gtk_signal_connect(GTK_OBJECT(stock_list), "select_row",
	  GTK_SIGNAL_FUNC(select_row_cb), NULL);
	gtk_signal_connect(GTK_OBJECT(stock_list), "unselect_row",
	  GTK_SIGNAL_FUNC(unselect_row_cb), NULL);
	gtk_container_add(GTK_CONTAINER(scroll_win), stock_list);
	gtk_clist_set_reorderable(GTK_CLIST(stock_list), TRUE);
	gtk_clist_set_use_drag_icons(GTK_CLIST(stock_list), FALSE);
	gtk_clist_column_titles_passive(GTK_CLIST(stock_list));
	gtk_clist_column_title_active(GTK_CLIST(stock_list), 0);
	gtk_clist_set_sort_column(GTK_CLIST(stock_list), 0);
	colbtn = gtk_clist_get_column_widget(GTK_CLIST(stock_list), 0)->parent;
	gtk_signal_connect(GTK_OBJECT(colbtn), "clicked",
		GTK_SIGNAL_FUNC(smbl_sort), NULL);			
	for (i = 0; i < NUMCOLS; i++) {
		gtk_clist_set_column_width(GTK_CLIST(stock_list), i, 50);
		gtk_clist_set_column_justification(GTK_CLIST(stock_list),i,GTK_JUSTIFY_CENTER);
	}
	gtk_widget_show(stock_list);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 0, 40);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 3, 100);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 7, 80);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 9, 100);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 10, 100);
	gtk_clist_set_column_width(GTK_CLIST(stock_list), 11, 80);			
}

/* Close a portfolio */

void close_portf(GtkWidget *widget, gpointer data) {
	gtk_clist_clear(GTK_CLIST(stock_list));
	gtk_window_set_title(GTK_WINDOW(window), "GNOME Portfolio Manager");
	if(!LoadPrev) {
	  gnome_config_push_prefix("GnomePM/");
	  gnome_config_set_string("Options/PfName", "\0");
	  gnome_config_sync();
	  gnome_config_pop_prefix();
	}
	if(update_id) {
	  gtk_timeout_remove(update_id);
	}
	no_pf_menus();
	set_pf_tools(FALSE);
}

/* Creates a new portfolio */

static void pf_ok_cb(GtkWidget *widget, gpointer data) {
	gchar *bleh;
	gchar *blah;
	
	blah = gtk_entry_get_text(GTK_ENTRY(data));
	gnome_config_push_prefix("GnomePM/");
	gnome_config_set_string("Options/PfName",blah);
	gnome_config_sync();
	pfname = gnome_config_get_string("Options/PfName");
	gnome_config_pop_prefix();

	if(update_id) {
	  gtk_timeout_remove(update_id);
	}
	if(UpdateTime > 0 && pfname[0] != '\0') {
	  update_id = gtk_timeout_add((UpdateTime * 60 * 1000), (GtkFunction) update_cb, NULL);
	}	
	gtk_clist_clear(GTK_CLIST(stock_list));
	fill_clist(stock_list, blah);
	bleh = (gchar *)g_malloc(strlen("GnomePM - ()") + strlen(blah) + 1);
			sprintf(bleh, "GnomePM - (%s)", blah);
			gtk_window_set_title(GTK_WINDOW(window), bleh);
		g_free(bleh);
	pf_menus();
	list_popup = gnome_popup_menu_new(symbol_popup);
	gnome_popup_menu_attach(list_popup, stock_list, NULL);
	set_symbol_popup(FALSE);
	set_pf_tools(TRUE);
gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
}

/* Asks for the Portfolio name to create */

void add_portfolio(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog, *portf_entry;

	dialog = gnome_dialog_new(_("New Portfolio"), GNOME_STOCK_BUTTON_OK,
		GNOME_STOCK_BUTTON_CANCEL, NULL);
	portf_entry = gtk_entry_new_with_max_length(255);
		gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), portf_entry);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(pf_ok_cb),
		portf_entry);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(portf_entry);
	gtk_widget_show(dialog);
}


static void add_portf(GtkWidget *widget, gpointer clist) {
	gchar *bleh, *blah;
	gchar *blh;
	gint w;
	
	if(GTK_CLIST(clist)->selection == NULL) {
	  return;
	}
	
	w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(clist)->selection)));
	gtk_clist_get_text(GTK_CLIST(clist), w, 0, &blh);
	fill_clist(stock_list, blh);
	blah = (gchar *)blh;

	gnome_config_push_prefix("GnomePM/");
	gnome_config_set_string("Options/PfName",blah);
	gnome_config_sync();
	pfname = gnome_config_get_string("Options/PfName");
	gnome_config_pop_prefix();

	bleh = (gchar *)g_malloc(strlen("GnomePM - ()") + strlen(blh) + 1);
		sprintf(bleh, "GnomePM - (%s)", blh);
		gtk_window_set_title(GTK_WINDOW(window), bleh);
	g_free(bleh);
	if(update_id) {
	  gtk_timeout_remove(update_id);
	}
	if(UpdateTime > 0 && !update_id && blh[0] != '\0') {
	  update_id = gtk_timeout_add((UpdateTime * 60 * 1000), (GtkFunction) update_cb, NULL);
	}
	fill_clist(stock_list, blh);
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
	pf_menus();
	list_popup = gnome_popup_menu_new(symbol_popup);
	gnome_popup_menu_attach(list_popup, stock_list, NULL);
	set_symbol_popup(FALSE);
	set_pf_tools(TRUE);
}

static void portfolio_add(char *file, GtkWidget *clist) {
	char **portf_name;
	portf_name = g_malloc(strlen(file) + 1);
	portf_name[0] = file;
	gtk_clist_append(GTK_CLIST(clist), portf_name);
	g_free(portf_name);
}

static void scan_portfdir(char *path, GtkWidget *clist)
{
	DIR *dir;
	int i;
	struct dirent *dirent;
	struct stat statbuf;
	char *file,*tmp;
	
	if((dir = opendir(path)))
	{
		i = 0;
		while((dirent = readdir(dir)))
		{
			if(strcmp(dirent->d_name,".")&&strcmp(dirent->d_name,".."))
			{
				file=(char *)g_malloc(dirent->d_reclen+strlen(path)+2);
				sprintf(file,"%s/%s",path,dirent->d_name);
				if(!lstat(file,&statbuf))
				{
					if(S_ISREG(statbuf.st_mode))
					{
						tmp=strrchr(file,'\0');
						if(tmp)
						{
							if(!strcasecmp(tmp,""))
							   {
								portfolio_add(dirent->d_name, clist);
								}
							else 
								g_free(file);
						}
						else
							g_free(file);
					}
					else
						g_free(file);
				}
				else
					g_free(file);
				i++;
			}
		}
		closedir(dir);
	}
}

void open_portf(GtkWidget *widget, gpointer data) {
	GtkWidget *portflist, *dialog, *scroll_win;
	char path[255];
	gchar *titles[] = {"Portfolio"};
	
	sprintf(path, "%s/.gnome-pm/quotes", getenv("HOME"));
	dialog = gnome_dialog_new(_("Open Portfolio"), GNOME_STOCK_BUTTON_OK,
		GNOME_STOCK_BUTTON_CANCEL, NULL);
		gtk_widget_set_usize(dialog, 240, 320), 
	scroll_win = gtk_scrolled_window_new(NULL, NULL);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_win),
				GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_widget_show(scroll_win);
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), scroll_win);
	portflist = gtk_clist_new_with_titles(1, titles);
		gtk_widget_show(portflist);
		scan_portfdir(path, portflist);
		gtk_container_add(GTK_CONTAINER(scroll_win), portflist);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(add_portf),
		portflist);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(dialog);
}

void get_update_symb(gchar *portf_name) {

	FILE *fp;
	char outfile[128];
	gchar *upd_url;
	gchar *symbol, *body;
	int i, len, success = 0;
	ghttp_request *m;

	/*
		/dj/quotes.csv?symbols=ORCL+AOL&format=st1l1cvbapomwery
	*/

		sprintf(outfile, "%s/.gnome-pm/quotes/%s", getenv("HOME"), portf_name);
		upd_url = (gchar *)g_malloc(sizeof(gchar *)* 256);
		sprintf(upd_url, "http://%s/dj/quotes.csv?symbols=", yh_host);
		for(i = 0; i < GTK_CLIST(stock_list)->rows; i++) {
		gtk_clist_get_text(GTK_CLIST(stock_list), i, 0, &symbol);
			sprintf(upd_url, "%s%s+", upd_url, symbol);
		}
		upd_url[strlen(upd_url) - 1] = upd_url[strlen(upd_url)];
		sprintf(upd_url, "%s&format=st1l1cvbapomwery&e=.csv", upd_url);

		m = ghttp_request_new();
		ghttp_set_uri(m, upd_url);
	if(ProxyHost) {
		ghttp_set_proxy(m, ProxyHost);
			if(ProxyUser) {
				if(ProxyPass) {
					ghttp_set_proxy_authinfo(m, ProxyUser, ProxyPass);
				}
			}
	}
		ghttp_prepare(m);
		ghttp_process(m);
		body = ghttp_get_body(m);
		len = ghttp_get_body_len(m);
		if(body) {
			if((fp = fopen(outfile, "w+")) != NULL) {
				fprintf(fp, body);
				fclose(fp);
				fill_clist(stock_list, portf_name);
			}
			else {
				fprintf(stderr, "ERROR: cannot open file for writing: %s\n", outfile);
			}
			success = 1;
		}
		else {
			fprintf(stderr, "ERROR: cannot access: %s\n", upd_url);
		}
		g_free(upd_url);
	while(gtk_events_pending())
		gtk_main_iteration();
}
