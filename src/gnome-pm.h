#include <gtk/gtk.h>
#include <gdk/gdktypes.h>
#include <gnome.h>
#include <ghttp.h>

#include <config.h>

#include "symbol.h"
#include "stock_list.h"
#include "menu.h"
#include "info.h"
#include "curr-conv.h"

#define NUMCOLS 14

char *pfname;

int RememberPosition, os_x, os_y, os_w, os_h, PromptOnQuit, SoundEnabled;
GtkWidget *statusbar, *window, *stock_list;
gchar *yh_id, *yh_pw, *yh_host, *ProxyHost, *ProxyUser, *ProxyPass;
int LoadPrev, UpdateTime;
guint update_id;

void pref_dlg_cb(GtkWidget *widget, gpointer data);
