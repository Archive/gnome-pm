#include <string.h>
#include "gnome-pm.h"
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>

int success = 0;

/*
 *	Here we just close a dialog when 'Cancel' is clicked.
 */

void cancelbt_cb(GtkWidget *widget, gpointer data) {
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
}

/* This is used to convert an inputted symbol to all uppercase */

static char *strupr(char *str) {
	int i;
	
	for(i = 0; i < strlen(str); i++) {
		if(str[i] - 32 >= 65 && str[i] - 32 <= 90) {
			str[i] = str[i] - 32 ;
		}
	}
	return(str);
}


/* Add a symbol found by Symbol Lookup */

static void add_fnd_smbl(GtkWidget *widget, gpointer clist) {
	GtkWidget *mbox;
	char *filenfo[NUMCOLS], *bleh, *blah;
	int i, w;

	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName=");
	gnome_config_pop_prefix();

	if(blah[0] == '\0') {
		mbox = gnome_message_box_new(_("No Portfolio Open!"),
			GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_OK,
			NULL);
		return;
	}
	if (GTK_CLIST(clist)->selection == NULL) {
		return;
	}
	else {
	w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(clist)->selection)));
	gtk_clist_get_text(GTK_CLIST(clist), w, 0, &bleh);

	filenfo[0] = strupr(bleh);
	for( i = 1; i < NUMCOLS; i++) {
		filenfo[i] = "N/A";
	}
	gtk_clist_append(GTK_CLIST(stock_list), filenfo);

	}
}

/* Do the symbol lookup stuff */

static void smb_lk_cb(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog, *clist, *scroll_win, *mbox;
	gchar *body, *urx[] = {"http://quote.yahoo.com/l?s=", "\0"};
	gchar *url, *smbl[2], *cell1, *cell2, *cell3, *cell4 = NULL;
	gchar *cell, *tbl, *bleh;
	gchar *url2 = g_strdup(gtk_entry_get_text(GTK_ENTRY(data)));
	gchar *titles[] = {"Symbol", "Name"};
	gint len, x, y;
	gchar *tbl_start = "<tr><td><a href=\"/q?s=";
	gchar *tbl_end = "</td></tr></table><p>Select";
	gchar *smbl_start = "<tr><td><a href=\"/q?s=";
	gchar *smbl_end = "\">";
	gchar *name_start = "</td><td>";
	gchar *name_end = "\n</td></tr>";
	ghttp_request *m;
	success = 0;

		dialog = gnome_dialog_new(_("Symbol Lookup Results"), _("Add Symbol"),
		GNOME_STOCK_BUTTON_CLOSE, NULL);
		gtk_widget_set_usize(dialog, 320, 240); 
	scroll_win = gtk_scrolled_window_new(NULL, NULL);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_win),
				GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_widget_show(scroll_win);
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), scroll_win);
	clist = gtk_clist_new_with_titles(2, titles);
		gtk_clist_set_column_width(GTK_CLIST(clist), 0, 50);
		gtk_widget_show(clist);
		gtk_container_add(GTK_CONTAINER(scroll_win), clist);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(add_fnd_smbl),
		clist);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);

	gtk_clist_freeze(GTK_CLIST(clist));
	for(x = 0;x < strlen(url2);x++) {
		if(url2[x] == ' ') {
			url2[x] = '+';
		}
	}
	url = g_strconcat(urx[0], url2, '\0');
	m = ghttp_request_new();
	ghttp_set_uri(m, url);
	ghttp_prepare(m);
	ghttp_process(m);
	body = ghttp_get_body(m);
	len = ghttp_get_body_len(m);
	if(body) {
	if(strstr(body, "returned too many matches")) {
	  mbox = gnome_warning_dialog(
	    _("Your search returned too many matches.\nPlease refine your search."));
	  gtk_widget_show(mbox);
	  return;
	}
	if(strstr(body, "returned no matches")) {
	  mbox = gnome_warning_dialog(
	    _("Your search returned no matches.\nPlease simplify your search."));
	  gtk_widget_show(mbox);
	  return;
	}
	cell = strstr(body, tbl_start);
	if (cell) {
		cell1 = strstr(body, tbl_end);
		if (cell1) {
			tbl = g_strndup(cell, ((cell1 + 10) - cell));
		if (tbl) {
		   while (tbl[0] != '\0') {
			cell1 = strstr(tbl, smbl_start);
			if (cell1) {
				cell2 = strstr(tbl, smbl_end);
				if (cell2) {
				smbl[0] = g_strndup(cell1 + strlen(smbl_start),
					cell2 - (cell1 + strlen(smbl_start)));
				}
			}
			cell3 = strstr(tbl, name_start);
			if (cell3) {
				cell4 = strstr(tbl, name_end);
				if (cell4) {
				smbl[1] = g_strndup(cell3 + strlen(name_start),
					cell4 - (cell3 + strlen(name_start)));
				}
			}
			gtk_clist_append(GTK_CLIST(clist), smbl);
			bleh = g_strndup(cell1, (cell4 + strlen(name_end) - cell1));
			y = strlen(bleh);
			for(x = 0; x < strlen(tbl); x++) {
				tbl[x] = tbl[x + y];
			}
		   }
		}

		}
		}
		success = 1;
	}
	gtk_clist_thaw(GTK_CLIST(clist));
	ghttp_request_destroy(m);
	if(!success) {
		gnome_appbar_set_status(GNOME_APPBAR(statusbar), _("Can't access Yahoo!(c) Finance"));
	}
	g_free(url);
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
	gtk_widget_show(dialog);
}

/* Dialog that asks for company to lookup a symbol for */

void lookup_cb(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog, *entry, *label;

	dialog = gnome_dialog_new(_("Symbol Lookup"), GNOME_STOCK_BUTTON_OK,
		GNOME_STOCK_BUTTON_CANCEL, NULL);
	label = gtk_label_new("Enter company name:");
	        gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), label);
	entry = gtk_entry_new_with_max_length(256);
		gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), entry);
	gnome_dialog_set_default(GNOME_DIALOG(dialog), 0);
	gnome_dialog_editable_enters(GNOME_DIALOG(dialog), GTK_EDITABLE(entry));
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(smb_lk_cb),
		entry);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(label);
	gtk_widget_show(entry);
	gtk_widget_grab_focus(GTK_WIDGET(GTK_EDITABLE(entry)));
	gtk_widget_show(dialog);
	
}

/* Adds a symbol to the list */

static void smb_ok_cb(GtkWidget *widget, gpointer data) {
	char *filenfo[NUMCOLS], *test = gtk_entry_get_text(GTK_ENTRY(data));
	int i;

	filenfo[0] = strupr(test);
	for( i = 1; i < NUMCOLS; i++) {
		filenfo[i] = "N/A";
	}
	gtk_clist_append(GTK_CLIST(stock_list), filenfo);
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
}

/* Asks for the symbol to add */

void add_symbol(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog, *entry;
	gchar *blah;
	
	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName=");
	gnome_config_pop_prefix();

	if(blah[0] == '\0')
		return;

	dialog = gnome_dialog_new(_("Add Symbol"), GNOME_STOCK_BUTTON_OK,
		GNOME_STOCK_BUTTON_CANCEL, NULL);
	entry = gtk_entry_new_with_max_length(10);
		gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), entry);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(smb_ok_cb),
		entry);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(entry);
	gtk_widget_show(dialog);
}

/* Removes the portfolio */

static void pf_rem_ok(GtkWidget *widget, gpointer data) {
	char temp[88];
	gchar *blah;

	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName");
	gnome_config_pop_prefix();

	gtk_clist_clear(GTK_CLIST(stock_list));
	sprintf(temp, "%s/.gnome-pm/portfolios/%s", getenv("HOME"), blah);
	remove(temp);
	sprintf(temp, "%s/.gnome-pm/quotes/%s", getenv("HOME"), blah);
	remove(temp);
	gtk_window_set_title(GTK_WINDOW(window), "GNOME Portfolio Manager");
	gnome_config_push_prefix("GnomePM/");
	gnome_config_set_string("Options/PfName", "\0");
	gnome_config_sync();
	gnome_config_pop_prefix();

	if(update_id) {
	  gtk_timeout_remove(update_id);
	}
	no_pf_menus();
	set_pf_tools(TRUE);
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
}

/* Asks if we really wanna delete the portfolio */
		
void del_portf_cb(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog;
	gchar *blah;
	
	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName=");
	gnome_config_pop_prefix();

	if(blah[0] == '\0')
		return;
		
	dialog = gnome_message_box_new(_("Are you sure you want to\nremove this portfolio?"),
	GNOME_MESSAGE_BOX_QUESTION, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(pf_rem_ok),
		NULL);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(dialog);
}

/* Removes a symbol from the list */

static void smb_rem_ok(GtkWidget *widget, gpointer data) {

	gtk_clist_remove(GTK_CLIST(stock_list),GPOINTER_TO_INT(GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
	
}

/* Asks if we really wanna remove the symbol */

void del_symb_cb(GtkWidget *widget, gpointer data) {
	GtkWidget *dialog;
	
	if(GTK_CLIST(stock_list)->selection == NULL)
		return;
	
	dialog = gnome_message_box_new(_("Are you sure you want to\n  remove this symbol?"),
		GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, GTK_SIGNAL_FUNC(smb_rem_ok),
		NULL);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, GTK_SIGNAL_FUNC(cancelbt_cb),
		NULL);
	gtk_widget_show(dialog);

}
