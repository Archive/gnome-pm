#include "gnome-pm.h" 
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>

int just_exit = 0;

static void validate_exist_dirs() {

	char temp[128], mkdir_cmd[128];
	DIR *dir;
	int i;
	
	for(i = 0; i < 3; i++) {
		if (i == 0) {
			sprintf(temp, "%s/.gnome-pm", getenv("HOME"));
		}
		else if (i == 1) {
			sprintf(temp, "%s/.gnome-pm/portfolios", getenv("HOME"));
		}
		else if (i == 2) {
			sprintf(temp, "%s/.gnome-pm/quotes", getenv("HOME"));
		}
		if (( dir = opendir(temp)) != NULL) {
		}
		else {
			sprintf(mkdir_cmd, "mkdir %s", temp);
			system(mkdir_cmd);
		}
	}
}


static gint save_state (GnomeClient        *client,
			gint                phase,
			GnomeRestartStyle   save_style,
			gint                shutdown,
			GnomeInteractStyle  interact_style,
			gint                fast,
			gpointer            client_data)
{
  gchar *session_id;
  gchar *argv[3];
  gint x, y, w, h;
  
  session_id = gnome_client_get_id(client);
  
  gdk_window_get_geometry(window->window, &x, &y, &w, &h, NULL);
  gnome_config_push_prefix(gnome_client_get_config_prefix(client));
  gnome_config_set_int ("Geometry/x", x);
  gnome_config_set_int ("Geometry/y", y);
  gnome_config_set_int ("Geometry/w", w);
  gnome_config_set_int ("Geometry/h", h);

  gnome_config_pop_prefix ();
  gnome_config_sync();

  argv[0] = (char*) client_data;
  argv[1] = "--discard-session";
  argv[2] = gnome_client_get_config_prefix (client);
  gnome_client_set_discard_command (client, 3, argv);
  gnome_client_set_clone_command (client, 1, argv);
  gnome_client_set_restart_command (client, 1, argv);

  return TRUE;
}


static gint die (GnomeClient *client, gpointer client_data)
{
  gtk_exit (0);
  return FALSE;
}


/*our general clean up and exit routine*/
static void delete_event (GtkWidget *widget, gpointer *data) {
	file_quit_callback(NULL, NULL);
}

static struct poptOption cb_options[] = {
  { NULL, '\0', 0, NULL, 0}
};

static void prepare_app() {
  GtkWidget *frame;
  gchar *bleh;

  window = gnome_app_new ("GnomePM", _("GNOME Portfolio Manager") );

  gtk_widget_realize (window);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), NULL);

  /*Allow ourselves to be grown and shrunk */
  gtk_window_set_policy( GTK_WINDOW (window),TRUE,TRUE,TRUE);
  
  gnome_config_push_prefix("GnomePM/");
  RememberPosition = gnome_config_get_int("Options/RememberPosition=1");
  if (RememberPosition) {
    os_x = gnome_config_get_int("Geometry/x=20");
    os_y = gnome_config_get_int("Geometry/y=20");
    os_w = gnome_config_get_int("Geometry/w=526");
    os_h = gnome_config_get_int("Geometry/h=261");
    gtk_window_set_default_size(GTK_WINDOW(window), os_w, os_h);
    gtk_widget_set_uposition(window, os_x, os_y);
  }
  else {
    gtk_window_set_default_size(GTK_WINDOW(window), 526, 261);
    gtk_widget_set_uposition(window, 20, 20);
  }
  PromptOnQuit = gnome_config_get_int("Options/PromptOnQuit=0");
  SoundEnabled = gnome_config_get_int("Sound/Enabled=1");
  yh_id = gnome_config_get_string("Yahoo/YahooID=\0");
  yh_pw = gnome_config_get_string("Yahoo/YahooPW=\0");
  yh_host = gnome_config_get_string("Yahoo/YahooHost=quote.yahoo.com");
  LoadPrev = gnome_config_get_int("Options/LoadPrev=0");
  pfname = gnome_config_get_string("Options/PfName=");
  ProxyHost = gnome_config_get_string("Yahoo/ProxyHost=\0");
  ProxyUser = gnome_config_get_string("Yahoo/ProxyUser=\0");
  ProxyPass = gnome_config_get_string("Yahoo/ProxyPass=\0");
  UpdateTime = gnome_config_get_int("Options/UpdateTime=5");

  gnome_config_pop_prefix();
  
  /* end config */
  
  gtk_container_border_width(GTK_CONTAINER(window), 1);
      
  /*create the containers*/
  frame = gtk_vbox_new(FALSE, 0);  
  gtk_container_border_width(GTK_CONTAINER(frame), 1);
  
  gnome_app_set_contents(GNOME_APP(window), frame);
  createToolbar(window);
  createClist(frame);
  statusbar = gnome_appbar_new(TRUE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar(GNOME_APP(window), statusbar);
  gtk_widget_show(statusbar);
  createMenu(window);

  if(LoadPrev) {
    fill_clist(stock_list, pfname);
	bleh = (gchar *)g_malloc(strlen("GnomePM - ()") + strlen(pfname) + 1);
		sprintf(bleh, "GnomePM - (%s)", pfname);
		gtk_window_set_title(GTK_WINDOW(window), bleh);
	g_free(bleh);
	pf_menus();
/*
	list_popup = gnome_popup_menu_new(symbol_popup);
	gnome_popup_menu_attach(list_popup, stock_list, NULL);
	set_symbol_popup(FALSE);
*/
	set_pf_tools(TRUE);
  }
  gtk_widget_show(frame);
  gtk_widget_show(window);
  
  if(UpdateTime > 0 && pfname[0] != '\0') {
    update_id = gtk_timeout_add((UpdateTime * 60 * 1000), (GtkFunction) update_cb, NULL);
  }
}

int main (int argc, char *argv[]) {
  
  GnomeClient *client;
  poptContext ctx;
  
  bindtextdomain(PACKAGE,GNOMELOCALEDIR);
  textdomain(PACKAGE);
  
  /* Check to see if ~/.gnome-pm/ exists, if not, make it */
  /* will be gone soon */
  validate_exist_dirs();
  
  /* Gnome SM stuff */
  gnome_init_with_popt_table("GnomePM", VERSION, argc, argv, cb_options, 0, &ctx);
  client= gnome_master_client();

  /* Arrange to be told when something interesting happens.  */
  gtk_signal_connect(GTK_OBJECT(client), "save_yourself",
  			GTK_SIGNAL_FUNC(save_state), (gpointer) argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (die), NULL);     
  if ( !just_exit )
    {
      prepare_app();
      
      gtk_main();
    }
  return 0;
}
