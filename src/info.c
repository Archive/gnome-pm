#include "gnome-pm.h"
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>


void news_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
  if(GTK_CLIST(stock_list)->selection == NULL) {
	return;
  }
  else {
	w=GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
	gtk_clist_get_text(GTK_CLIST(stock_list), w, 0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/n/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);
  }
}

void chart_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
		}
	else {
		w=GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list), w, 0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://finance.yahoo.com/q?s=", blah, "&d=1y", NULL);
	gnome_url_show(url);
	g_free(url);

	}
}

void sec_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w=GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/e/l/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);
	}
}

void mesg_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w=GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://messages.yahoo.com/?action=q&board=", blah, NULL);
	gnome_url_show(url);
	g_free(url);

	}
}

void profl_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/p/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);

	}
}

void rsrch_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;

	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/z/a/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);
	}
}

void insdr_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	
	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/t/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);

	}
}

void financials_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;

	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
	w = GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list), w, 0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/fin/l/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);
	}
}

void updgrade_cb(GtkWidget *widget, gpointer data) {
	gchar *blah, *url, *tmp;
	gint w, x;
	

	if(GTK_CLIST(stock_list)->selection == NULL) {
		return;
	}
	else {
		w =	GPOINTER_TO_INT((GTK_CLIST_ROW(GTK_CLIST(stock_list)->selection)));
		gtk_clist_get_text(GTK_CLIST(stock_list),w,0, &blah);
	tmp = g_strdup(blah);
	for(x = strlen(blah); x > 0; x--) {
	  tmp[x] = '\0';
	}
	url = g_strconcat("http://biz.yahoo.com/c/", tmp, "/", blah, ".html", NULL);
	gnome_url_show(url);
	g_free(url);

	}
}
