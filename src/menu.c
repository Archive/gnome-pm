#include "gnome-pm.h"
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>

static void doQuit(GtkWidget *widget, gpointer *blah) {
  gdk_window_get_position(window->window,&os_x,&os_y);
  gdk_window_get_size(window->window,&os_w,&os_h);
  
  gnome_config_push_prefix("GnomePM/");
  
  if (RememberPosition) {
    gnome_config_set_int("Geometry/x", os_x);
    gnome_config_set_int("Geometry/y", os_y);
    gnome_config_set_int("Geometry/w", os_w);
    gnome_config_set_int("Geometry/h", os_h);
  }
  if (!LoadPrev) {
    gnome_config_set_string("Options/PfName","\0");
  }
  
  gnome_config_sync();
  gnome_config_pop_prefix();
  
  gtk_main_quit();
}

void file_quit_callback (GtkWidget *widget, gpointer data){
  
  GtkWidget *mbox;
  
  if (PromptOnQuit) {
  	mbox = gnome_message_box_new(_("Are you sure you want to exit?"),
  			GNOME_MESSAGE_BOX_QUESTION, GNOME_STOCK_BUTTON_YES,
  			GNOME_STOCK_BUTTON_NO, NULL);
  	gnome_dialog_button_connect(GNOME_DIALOG(mbox), 0, doQuit, NULL);
  	gtk_widget_show(mbox);
  }
  else {
    doQuit(NULL, NULL);
  }
}

gint update_cb(GtkWidget *widget, gpointer data){
	gchar *blah;
	
	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName=");
	gnome_config_pop_prefix();
	
	if(blah[0] == '\0')
		return FALSE;
	else {
	 get_update_symb(blah);
	 return TRUE;
	}
}

void save_cb(GtkWidget *widget, gpointer data){
	gchar *blah;
	
	gnome_config_push_prefix("GnomePM/");
	blah = gnome_config_get_string("Options/PfName=");
	gnome_config_pop_prefix();
	
	if(blah[0] == '\0')
		return;

	get_update_symb(blah);
}

static void help_yahoo_callback (GtkWidget *widget, void *data) {
	gnome_url_show("http://quote.yahoo.com/portf/disclaim.html");
}

static void help_about_callback (GtkWidget *widget, void *data) {
  GtkWidget *about;
  const gchar *authors[] = {
    "Rodney Dawes <dobey@free.fr>",
    NULL
  };
  
  about = gnome_about_new (_("GNOME Portfolio Manager"),
			    VERSION,
			    /* copyright notice */
			    "Copyright 1999-2002 (C) Elysium GNU/Linux",
			    authors,
			    /* other comments */
			    _("Portfolio management utility for GNOME\n"
			      "Information provided by Yahoo!(C) Finance\n"
			      "This program is not supported by Yahoo!(c)."),
			   "gnome-pm/about.jpg");
  gtk_widget_show (about);
  
  return;
}


GtkMenuFactory *create_menu ();

static GnomeUIInfo program_menu[]= {
 { 
    GNOME_APP_UI_ITEM,
    _("_Preferences"), _("Edit Portfolio Manager Settings"),
    pref_dlg_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    0, GDK_CONTROL_MASK, NULL
 },
 GNOMEUIINFO_SEPARATOR,
 /*
 {
   GNOME_APP_UI_ITEM,
   _("Currency Converter"), _("Convert between currencies"),
   curr_conv_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
   '4', GDK_CONTROL_MASK, NULL
 },
 */
 { 
    GNOME_APP_UI_ITEM,
    _("E_xit"), _("Exit Portfolio Manager"),
    file_quit_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
    'Q', GDK_CONTROL_MASK, NULL
 },
 GNOMEUIINFO_END
};

static GnomeUIInfo portf_menu[]= {
  {
    GNOME_APP_UI_ITEM,
    _("_New..."), _("Create New Portfolio"),
    add_portfolio, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    'N', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Open"), _("Open a Portfolio"),
    open_portf, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    'O', GDK_CONTROL_MASK, NULL
  },
  {
	GNOME_APP_UI_ITEM,
	_("_Save"), _("Save the current portfolio"),
	save_cb, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE,
	'S', GDK_CONTROL_MASK, NULL
  }, 
  { 
    GNOME_APP_UI_ITEM,
    _("_Close"), _("Close the Current Portfolio"),
    close_portf, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE,
    'W', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Delete"), _("Delete Current Portfolio"),
    del_portf_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TRASH,
    'D', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  { 
    GNOME_APP_UI_ITEM,
    _("Sort Symbols"), _("Sort the Portfolio"),
    smbl_sort, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_INDEX,
    'S', GDK_SHIFT_MASK, NULL
  },
  { 
    GNOME_APP_UI_ITEM,
    _("_Update"), _("Update Listings"),
    update_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_REFRESH,
    'U', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo more_info_menu[] =
{
  {
    GNOME_APP_UI_ITEM,
    _("_Chart"), _("View charts for the company"),
    chart_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_INDEX,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_News"), _("View news for the company"),
    news_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SEARCH,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_SEC"), _("View SEC info for the company"),
    sec_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Messages"), _("View messages for the company"),
    mesg_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_MAIL,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Profile"), _("View company profile"),
    profl_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Research"), _("Research the company"),
    rsrch_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BOOK_OPEN,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Insider"), _("View company insider"),
    insdr_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_JUMP_TO,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Financials"), _("View company finance balance sheet"),
    financials_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ATTACH,
    0, GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Up/Downgrades"), _("View company upgrades and downgrades"),
    updgrade_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UP,
    0, GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo symbol_menu[]=
{
  {
    GNOME_APP_UI_ITEM,
    _("_Add..."), _("Add Symbol to Portfolio"),
    add_symbol, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_ADD,
    'A', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("_Remove"), _("Remove Symbol from Portfolio"),
    del_symb_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REMOVE,
    'R', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    _("Symbol _Lookup"), _("Lookup a Symbol"),
    lookup_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SEARCH,
    'L', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    _("Move _Up"), _("Move the Symbol up one space"),
    smbl_up, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UP,
    'U', GDK_SHIFT_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("Move _Down"), _("Move the Symbol down one space"),
    smbl_dn, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_DOWN,
    'D', GDK_SHIFT_MASK, NULL
  },
  /*
  {
    GNOME_APP_UI_ITEM,
    _("Mod_ify..."), _("Modify Symbol Settings"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP,
    'I', GDK_CONTROL_MASK, NULL
  },
  */
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[]=
{
  {
    GNOME_APP_UI_ITEM,
    _("Yahoo! Disclaimer"), _("Important Disclaimer and Copyright"),
    help_yahoo_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    _("A_bout..."), _("Info about GNOME Portfolio Manager"),
    help_about_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    0, 0, NULL
  }, 
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[]= 
{
  GNOMEUIINFO_SUBTREE(_("_Program"), program_menu),
  GNOMEUIINFO_SUBTREE(_("P_ortfolio"), portf_menu),
  GNOMEUIINFO_SUBTREE(_("_Symbol"), symbol_menu),
  GNOMEUIINFO_SUBTREE(_("More _Info"), more_info_menu),
  GNOMEUIINFO_SUBTREE(_("_Help"), help_menu),
  GNOMEUIINFO_END
};

void no_pf_menus(void) {
  gtk_widget_set_sensitive(portf_menu[2].widget, FALSE);
  gtk_widget_set_sensitive(portf_menu[3].widget, FALSE);
  gtk_widget_set_sensitive(portf_menu[4].widget, FALSE);
  gtk_widget_set_sensitive(portf_menu[6].widget, FALSE);
  gtk_widget_set_sensitive(portf_menu[7].widget, FALSE);
  gtk_widget_set_sensitive(main_menu[2].widget, FALSE);
}

void pf_menus(void) {
  gtk_widget_set_sensitive(portf_menu[2].widget, TRUE);
  gtk_widget_set_sensitive(portf_menu[3].widget, TRUE);
  gtk_widget_set_sensitive(portf_menu[4].widget, TRUE);
  gtk_widget_set_sensitive(portf_menu[6].widget, TRUE);
  gtk_widget_set_sensitive(portf_menu[7].widget, TRUE);
  gtk_widget_set_sensitive(main_menu[2].widget, TRUE);
  gtk_widget_set_sensitive(symbol_menu[1].widget, FALSE);
  gtk_widget_set_sensitive(symbol_menu[5].widget, FALSE);
  gtk_widget_set_sensitive(symbol_menu[6].widget, FALSE);
}

void select_row_cb(GtkWidget *clist, gint row, gint col, gpointer event) {
  gtk_widget_set_sensitive(main_menu[3].widget, TRUE);
  gtk_widget_set_sensitive(symbol_menu[1].widget, TRUE);
  gtk_widget_set_sensitive(symbol_menu[5].widget, TRUE);
  gtk_widget_set_sensitive(symbol_menu[6].widget, TRUE);
  set_symbol_popup(TRUE);
  set_symbol_tools(TRUE);
}

void unselect_row_cb(GtkWidget *clist, gint row, gint col, gpointer event) {
  gtk_widget_set_sensitive(main_menu[3].widget, FALSE);
  gtk_widget_set_sensitive(symbol_menu[1].widget, FALSE);
  gtk_widget_set_sensitive(symbol_menu[5].widget, FALSE);
  gtk_widget_set_sensitive(symbol_menu[6].widget, FALSE);
  set_symbol_popup(FALSE);
  set_symbol_tools(FALSE);
}

void createMenu(GtkWidget *frame)
     
{
  gnome_app_create_menus (GNOME_APP (frame), main_menu);
  gnome_app_install_menu_hints(GNOME_APP(frame), main_menu);
  gtk_widget_set_sensitive(main_menu[3].widget, FALSE);
  gtk_widget_set_sensitive(main_menu[2].widget, FALSE);
  no_pf_menus();
}

